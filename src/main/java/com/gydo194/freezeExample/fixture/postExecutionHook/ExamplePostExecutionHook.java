package com.gydo194.freezeExample.fixture.postExecutionHook;

import com.gydo194.freeze.Exception.HookException;
import com.gydo194.freeze.Hook.HookInterface;

public final class ExamplePostExecutionHook implements HookInterface {

	public String getName() {
		return "Example Post-Execution Hook";
	}

	public void run() throws HookException {
		/*
		 * Execute the action for which this hook is responsible here.
		 * 
		 * Use this to close database connections, shut down an ORM etc.
		 */
	}

}
