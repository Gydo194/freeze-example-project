package com.gydo194.freezeExample.fixture.fixture;

import com.gydo194.freeze.Exception.FixtureException;
import com.gydo194.freeze.Fixture.FixtureInterface;
import com.gydo194.freezeExample.Entity.User;

public final class ExampleFixture implements FixtureInterface {

	/**
	 * Generate the default user
	 * 
	 * @return {@link User} The application's default user
	 */
	private User generateDefaultUser() {
		User defaultUser = new User();

		defaultUser.setUsername("admin");
		defaultUser.setPassword("hunter2");
		
		return defaultUser;
	}

	public String getName() {
		return "Example Fixture";
	}

	public void load() throws FixtureException {
		User defaultUser = generateDefaultUser();
		
		/*
		 * Create a database session
		 * Create a transaction
		 * Save the defaultUser object to the database
		 * Commit the transaction
		 * Close the session
		 */
	}

}
