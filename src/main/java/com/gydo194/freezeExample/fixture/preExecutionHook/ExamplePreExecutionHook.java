package com.gydo194.freezeExample.fixture.preExecutionHook;

import com.gydo194.freeze.Exception.HookException;
import com.gydo194.freeze.Hook.HookInterface;

public final class ExamplePreExecutionHook implements HookInterface {

	public String getName() {
		return "Example Pre-Execution Hook";
	}

	public void run() throws HookException {
		/*
		 * Execute the action for which this hook is responsible here.
		 * 
		 * Use this to connect to a database, initialize an ORM etc.
		 */
	}

}
