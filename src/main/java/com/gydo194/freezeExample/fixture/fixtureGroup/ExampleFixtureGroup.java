package com.gydo194.freezeExample.fixture.fixtureGroup;

import java.util.LinkedList;
import java.util.Queue;

import com.gydo194.freeze.Fixture.FixtureGroupInterface;
import com.gydo194.freeze.Fixture.FixtureInterface;
import com.gydo194.freeze.Hook.HookInterface;
import com.gydo194.freezeExample.fixture.fixture.ExampleFixture;
import com.gydo194.freezeExample.fixture.postExecutionHook.ExamplePostExecutionHook;
import com.gydo194.freezeExample.fixture.preExecutionHook.ExamplePreExecutionHook;

public final class ExampleFixtureGroup implements FixtureGroupInterface {

	public String getName() {
		return "Example Fixture Group";
	}

	public Queue<FixtureInterface> getFixtures() {
		Queue<FixtureInterface> fixtures = new LinkedList<FixtureInterface>();
		fixtures.add(new ExampleFixture());
		return fixtures;
	}

	public Queue<HookInterface> getPreExecutionHooks() {
		Queue<HookInterface> preExecutionHooks = new LinkedList<HookInterface>();
		preExecutionHooks.add(new ExamplePreExecutionHook());
		return preExecutionHooks;
	}

	public Queue<HookInterface> getPostExecutionHooks() {
		Queue<HookInterface> postExecutionHooks = new LinkedList<HookInterface>();
		postExecutionHooks.add(new ExamplePostExecutionHook());
		return postExecutionHooks;
	}

}
